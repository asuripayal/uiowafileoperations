using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
class FileOperations
{

    public static void Main(string[] args)
    {
        
        string inputfile1 = @"CombinedLetters\Input\Admission\";
        string inputfile2 = @".\CombinedLetters\Input\Scholarship\";
        string resultfile = @".\CombinedLetters\Output\";


        LetterService letterServiceObj = new LetterService();
        letterServiceObj.CombineTwoLetters(inputfile1, inputfile2, resultfile);
    }

    /// <summary>
    /// Archiving Files from Input to Archive.
    /// </summary>
    /// <param name="inputFolder"> Input folder represents the academic or scholarship folder from Input directory.</param>
    /// <param name="outputFolder"> Output folder represents the achademic or scholarship frolder in Archive directory.</param>
    /// <returns>Returns boolean value to indicate archiving status.</returns>
    public static bool ArchiveFile(string inputFolder, string outputFolder)
    {
        bool result = false; // variable to represnt successful/unsuccessful archival status.
        try
        {
            if (Path.Exists(inputFolder))
            {
                string[] files = Directory.GetFiles(inputFolder);
                int inputFileLength = files.Length;
                foreach (string file in files)
                {
                    File.Move(file, outputFolder+ Path.GetFileName(file));
                    
                }
                files = Directory.GetFiles(inputFolder);
                if(files.Length == 0)
                {
                    Directory.Delete(inputFolder, true);
                }
                string[] destFiles = Directory.GetFiles(outputFolder);
                if (files.Length == 0 && destFiles.Length > 0) 
                    result = true;  
            }

            return result;
        }
        catch (Exception e)
        {
            Console.WriteLine("Error occured while archiving file - " + e.Message);
            return result;
        }
    }

}



public interface ILetterService
{
    /// <summary>
    ///  Combine 2 letters into 1 file.
    /// </summary>
    /// <param name="inputfile1"> File path for first letter.</param>
    /// <param name="inputfile2"> File path for second letter.</param>
    /// <param name="resultfile"> File path for combined letter.</param>
    void CombineTwoLetters(string inputfile1, string inputfile2, string resultfile);
}

public class LetterService:ILetterService
{
    public void CombineTwoLetters(string inputfile1, string inputfile2, string resultfile)
    {
        //to fetch all dates of last week and store in a list. (We are performing process letters functionality for last 7 days in
        //case scheduler run fails.)
        List<string> allDates = FetchAllDates(7);
        foreach (string date in allDates)
        {
            string folderNameAdmission = Path.GetFullPath(inputfile1) + date;
            string folderNameScholarship = Path.GetFullPath(inputfile2) + date;

            

            List<string> studentList = new List<string>();
            if (Path.Exists(folderNameAdmission) && Path.Exists(folderNameScholarship))
            {
                Console.WriteLine("Performing CombineLetters Operation for date - " + date);
                //creating a new folder in Output directory
                //string currentDirectory = Directory.GetCurrentDirectory();
                string resultfileFolder = Path.GetFullPath(resultfile) + date + "\\";
                Directory.CreateDirectory(resultfileFolder);

                List<String> admissionPathList = Directory.GetFiles(folderNameAdmission).ToList<String>();
                string[] scholarshipPathList = Directory.GetFiles(folderNameScholarship);

                foreach (var scholarshipPath in scholarshipPathList)
                {
                    string fileName = Path.GetFileName(scholarshipPath);
                    string fileNamesSubset = fileName.Substring(12);
                    if (admissionPathList.FindIndex(str => str.Contains(fileNamesSubset)) != -1)
                    {
                        //Admission letter in Input/Admission/yyymmdd folder for same student. Performing merging of 2 files
                        int admissionfilePathIndex = admissionPathList.FindIndex(str => str.Contains(fileNamesSubset));
                        string scholarshipFileContent = File.ReadAllText(scholarshipPath);
                        string admissionFileContent = File.ReadAllText(admissionPathList[admissionfilePathIndex]);
                        string newFileContent = admissionFileContent + "\n" + scholarshipFileContent;

                        string outputFilePath = resultfileFolder + fileNamesSubset;
                        File.WriteAllText(outputFilePath, newFileContent);
                        Console.WriteLine("New file has been created and contents of admission and scholarship file has been merged.");

                    }
                    else
                        Console.WriteLine("Admission letter not found for student - " + fileNamesSubset);
                    fileNamesSubset = fileNamesSubset.Remove(fileNamesSubset.Length - 4,4);
                    studentList.Add(fileNamesSubset); // adding all student ID's to a list for testReport generation
                }

                // defining variables for archival process
                string archiveAdmission = @".\\CombinedLetters\Archive\Admission\";
                string archiveScholarship = @".\\CombinedLetters\Archive\Scholarship\";
                string archiveAdmissionfolder = Path.GetFullPath(archiveAdmission) + date + "\\";
                Directory.CreateDirectory(archiveAdmissionfolder);
                string archiveScholarshipfolder = Path.GetFullPath(archiveScholarship) + date + "\\";
                Directory.CreateDirectory(archiveScholarshipfolder);

                // archiving the working date folder to Archive/AdmissionLetter and Arhive/ScholarshipLetter
                FileOperations.ArchiveFile(folderNameAdmission, archiveAdmissionfolder);
                FileOperations.ArchiveFile(folderNameScholarship, archiveScholarshipfolder);
                Console.WriteLine("Admission and scholarship files have been moved from Input folder to Archive folder and directory is also deleted from input folder.");

                // Task 3 - Creating Text report
                CreateTextReport(date, resultfileFolder, studentList);
                

            }
        }
        

    }

    /// <summary>
    /// To fetch all dates for last week.
    /// </summary>
    /// <param name="days">Indicate the no of days required.</param>
    /// <returns>Returns List of string for all last week's data.</returns>
    public List<string> FetchAllDates(int days)
    {
        DateTime todayDate = DateTime.Today;
        string formatDate = todayDate.ToString("yyyyMMdd");
        // 1 week before date
        DateTime weekBeforeDate = todayDate.AddDays(-days);
        // creating a list to store all dates in yyyymmdd format.
        List<string> allDates = new List<string>();
        allDates.Add(formatDate);
        for (int i = 0; i <days; i++)
        {
            DateTime currentDateMinusDays = weekBeforeDate.AddDays(i);
            string formatDateNew = currentDateMinusDays.ToString("yyyyMMdd");
            allDates.Add(formatDateNew);
        }
        allDates.Sort();
        allDates.Reverse(); // We are sorting in descending order to work on the current date first.
        return allDates;
    }

    /// <summary>
    /// Creating Test Report for Task 3 
    /// </summary>
    /// <param name="date"></param>  Current data which needs to be added in the report.
    /// <param name="resultFolder"></param> Final folder where report has to be placed.
    /// <param name="studentList"></param> List of student ID's which needs to be added in the report.
    private static void CreateTextReport(string date, string resultFolder, List<string> studentList)
    {
        try
        {
            string formattedDate = "";
            studentList.Sort(); // adding all the student ID's in report in ascending order.
            if (DateTime.TryParseExact(date, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out DateTime newDate))
            {
                formattedDate = newDate.ToString("yyyy/MM/dd");
            }
            string reportFileName = "TestReport-" + date + ".txt";
            string outputPath = Path.Combine(resultFolder, reportFileName);

            string reportText = $"{formattedDate} Report{Environment.NewLine}------------------------{Environment.NewLine}{Environment.NewLine}";
            reportText += $"No of combined Letters: {studentList.Count}{Environment.NewLine}";
            reportText += string.Join(Environment.NewLine, studentList);

            File.WriteAllText(outputPath, reportText);
            Console.WriteLine($"Created test report for date - {date} in output folder.");
            Console.ReadKey(); // to prevent to console window from closing automatically.
        }
        catch (Exception e)
        {
            Console.WriteLine("Error creating text report: " + e.Message);
        }
    }
}
