#Steps to test the application

**This guide provides step-by-step instructions to test and use the File Operations application.**

**Before you begin, make sure you have the following:**

- Git installed on your machine.
- .NET SDK (Software Development Kit) installed. You can download it from [dotnet.microsoft.com](https://dotnet.microsoft.com/download).

# Testing Steps:

1. First clone the application into local using git clone command: **git clone https://gitlab.com/asuripayal/uiowafileoperations.git**

2. Navigate to the specific folder under your own working copy of the project in local: **\FileOperationsApp\bin\Debug\net7.0\CombinedLetters\Input**

3. In the above path, create 1 folder under Admission and Scholarship each: Follow the naming convention for folder:
-            Name it as **yyyymmdd** (this folder represents the current system date for which you want to test)

4. Add few .txt files in both of the above created folders. (Example: Admission\20230810 and Scholarship\20230810)
-            Name the files as **Admission-********.txt** and **Scholarship-********.txt**

5. There are 2 ways to run the application :
-            Load the solution into visual studio and run
-            Through command prompt: Navigate to **\FileOperationsApp\bin\Debug\net7.0\** and use command: **dotnet FileOperationsApp.dll**

6. Follow the logs printed on the command line window.

7. To verify successful run, check the following:
-            Go to  **\FileOperationsApp\bin\Debug\net7.0\CombinedLetters\Output** folder and check for the new folder with the same name that you created in Input folder.
-            If there were any matching files in Admission and Scholarship for same student, check if in the new folder of output directory a new file has been created with file name as **StudentID**
-            Open the .txt file and check whether the contents of file present in Admission and Scholarship in input folder has been merged.
-            Verify whether test report file has been created with name: "**TestReport-yyyymmdd**".
-            Verify whether the files from **Input/Admission/yyyymmdd** and **Input/Scholarship/yyyymmdd** have been moved successfully to Archive folder.
